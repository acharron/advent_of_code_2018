import java.io.File

fun main(args: Array<String>) {
    val input = File("src/input_01.txt").readLines()

    var result = 0

    for (row in input) {
        if (row.startsWith("+")) {
            result += row.substring(1).toInt()
        } else {
            result -= row.substring(1).toInt()
        }
    }

    println("Part 1 : $result")

    var result2 = 0
    var loop = 0
    var buffer = 0
    val seenFrequencies = arrayListOf<Int>()

    while (result2 == 0 && loop < 1000) {
        for (row in input) {
            seenFrequencies.add(buffer)

            if (row.startsWith("+")) {
                buffer += row.substring(1).toInt()
            } else {
                buffer -= row.substring(1).toInt()
            }

            if (seenFrequencies.contains(buffer)) {
                result2 = buffer
                break
            }
        }

        loop++
    }

    println("Part 2 = $result2")
}