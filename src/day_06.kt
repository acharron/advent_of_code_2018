import java.io.File
import kotlin.math.abs

data class Beacon(val id: Int, val x: Int, val y: Int, var infinite: Boolean)

val nullBeacon = Beacon(-1, -1, -1, true)

fun main(args: Array<String>) {
    val input = File("src/input_06.txt").readLines()
    val maxXY = 1000

    val beacons = arrayListOf<Beacon>()

    var id = 1
    for (line in input) {
        val coord = line.split(", ").map { it.toInt() }
        val x = coord[0]
        val y = coord[1]

        beacons.add(Beacon(id, x, y, false))

        id++
    }


    val gridClosest = mutableMapOf<Pair<Int, Int>, Beacon>()
    val gridTotalDistance = mutableMapOf<Pair<Int, Int>, Int>()
    for (i in 0..maxXY) {
        for (j in 0..maxXY) {
            gridClosest[Pair(i, j)] = findClosestBeacon(i, j, beacons)

            // Part 2
            var total = 0
            for (b in beacons) {
                total += abs(b.x - i) + abs(b.y - j)
            }
            gridTotalDistance[Pair(i, j)] = total
        }
    }


    // Ajouter les -1 et maxXY+1, voir quels sont les beacons infinis
    for (i in -1..maxXY) {
        findClosestBeacon(i, -1, beacons).infinite = true
        findClosestBeacon(i, maxXY + 1, beacons).infinite = true
    }
    for (j in -1..maxXY) {
        findClosestBeacon(-1, j, beacons).infinite = true
        findClosestBeacon(maxXY + 1, j, beacons).infinite = true
    }


    val aaa = gridClosest.filter { !it.value.infinite }

    val largest: Int?
    val areas = arrayListOf<Int>()
    for (b in beacons) { areas.add(0) }

    for (cell in aaa) {
        areas[cell.value.id]++
    }
    largest = areas.max()

    println("Part 1 : $largest")


    val safeArea = gridTotalDistance.filter { it.value < 10000 }.size
    println("Part 2 : $safeArea")
}


fun findClosestBeacon(i: Int, j: Int, beacons: ArrayList<Beacon>): Beacon {
    var closestBeacon = nullBeacon
    var min = 1000

    for (b in beacons) {
        val distance = abs(b.x - i) + abs(b.y - j)
        if (distance < min) {
            min = distance
            closestBeacon = b
        } else if (distance == min) {
            // Same distance to 2 beacons : doesn't count
            closestBeacon = nullBeacon
        }
    }

    return closestBeacon
}