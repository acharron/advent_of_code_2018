import java.io.File

fun main(args: Array<String>) {
    val input = File("src/input_05.txt").readLines()
//    val input = File("src/input_05_ex.txt").readLines()

    val line = input[0]

    println("Part 1 : ${reactAndGetLength(line)}")


    // Part 2
    // {a: 15}, {b: 53}, {c: 42}, ...
    val mapResult = mutableMapOf<Char, Int>()

    val chars = "abcdefghijklmnopqrstuvwxyz".toCharArray()
    for (c in chars) {
        val newPoly = removeLetter(line, c)
        val len = reactAndGetLength(newPoly)

        mapResult[c] = len
    }

    print("Part 2 : ")
    println(mapResult.minBy { it.value })
}

fun removeLetter(line: String, letter: Char) : String {
    val up = letter.toUpperCase()
    val reg = Regex("$letter|$up")
    val res = reg.replace(line, "")

    println("Removed $letter : $res")

    return res
}

fun reactAndGetLength(line: String) : Int {
    var res = line
    var didReact : Boolean

    do {
        didReact = false
        var newLine = res
        var j = 0

        var i = 0

        while (i < res.length -1) {
            val c = res[i]
            val next = res[i + 1]


            if (c != next && c.toLowerCase() == next.toLowerCase()) {
                didReact = true

                newLine = newLine.removeRange(i - j, i - j + 2)
                i++
                j += 2
            }



            i++
        }

        res = newLine
    } while (didReact)


    return res.length
}