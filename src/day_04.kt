import java.io.File

enum class State {
    START, SLEEP, WAKE
}

data class Report(val year: Int, val day: String, val hour: Int, val min: Int, val id: Int, val state: State)

val patternGuard = Regex("""^\[(\d+)-(\d+-\d+) (\d+):(\d+)] Guard #(\d+)""")
val patternSleep = Regex("""^\[(\d+)-(\d+-\d+) (\d+):(\d+)] falls""")
val patternWakes = Regex("""^\[(\d+)-(\d+-\d+) (\d+):(\d+)] wakes""")


fun main(args: Array<String>) {
    val input = File("src/input_04.txt").readLines()
    val sorted = input.sorted()

    val reports = arrayListOf<Report>()


    var lastGuardId = 0

    for (line in sorted) {
        if (patternGuard.containsMatchIn(line)) {
            // Add report with new guard
            val result = patternGuard.find(line)
            if (result != null) {
                val (year, day, hour, min, id) = result.destructured.toList()

                reports.add(Report(year.toInt(), day, hour.toInt(), min.toInt(), id.toInt(), State.START))

                lastGuardId = id.toInt()
            }
        } else if (patternSleep.containsMatchIn(line)) {
            // Add report with last id
            val result = patternSleep.find(line)
            if (result != null) {
                val (year, day, hour, min) = result.destructured.toList()

                reports.add(Report(year.toInt(), day, hour.toInt(), min.toInt(), lastGuardId, State.SLEEP))
            }
        } else if (patternWakes.containsMatchIn(line)) {
            // Add report with last id
            val result = patternWakes.find(line)
            if (result != null) {
                val (year, day, hour, min) = result.destructured.toList()

                reports.add(Report(year.toInt(), day, hour.toInt(), min.toInt(), lastGuardId, State.WAKE))
            }
        }
    }

    val guardsMinutes = mutableMapOf<Int, MutableMap<Int, Int>>()
    val guardsTotal = mutableMapOf<Int, Int>()

    var lastReport = Report(0, "0-0", 0, 0, -123, State.START)

    for (report in reports) {
        if (guardsMinutes[report.id] == null) {
            guardsMinutes[report.id] = mutableMapOf()
        }
        if (guardsTotal[report.id] == null) {
            guardsTotal[report.id] = 0
        }


        val minutes = guardsMinutes[report.id] ?: mutableMapOf()
        var total = guardsTotal[report.id] ?: 0


        if (report.state == State.SLEEP) {
            val i = minutes[report.min] ?: 0
            minutes[report.min] = i + 1

            total++

            lastReport = report
        }


        if (report.state == State.WAKE) {
            val lastMin = lastReport.min

            for (i in lastMin + 1 until report.min) {
                val prev = minutes[i] ?: 0

                minutes[i] = prev + 1

                total++
            }
        }


        guardsTotal[report.id] = total

    }



    val topGuard = guardsTotal.maxBy { it.value }
    val topGuardTopMin = guardsMinutes[topGuard?.key]?.maxBy { it.value }

    println("Part 1 : Most asleep total guard : $topGuard")
    println("Part 1 : Most asleep minute : $topGuardTopMin")

    val topId = topGuard?.key ?: -1
    val topMin = topGuardTopMin?.key ?: -1

    println("Part 1 : ${topId * topMin}")




    // Part 2
    val maxMinPerGuard = mutableMapOf<Int, Int>()

    for (guardMin in guardsMinutes) {
        val maxMin = guardMin.value.maxBy { it.value }

        maxMinPerGuard[guardMin.key] = maxMin?.value ?: -1
    }

    val guardAlwaysSameMinuteId = maxMinPerGuard.maxBy { it.value }
    val guardAlwaysSameMinuteMin = guardsMinutes[guardAlwaysSameMinuteId?.key]?.maxBy { it.value }

    println("Part 2 : $guardAlwaysSameMinuteId $guardAlwaysSameMinuteMin")

}