import java.io.File

data class Claim(val id: Int,
                 val left: Int, val top: Int,
                 val w: Int, val h: Int,
                 var overlap: Boolean)

fun main(args: Array<String>) {
    val input = File("src/input_03.txt").readLines()
//    val input = File("src/input_03_ex.txt").readLines()


    val area = mutableMapOf<Pair<Int, Int>, Int>()
    val owners = mutableMapOf<Pair<Int, Int>, ArrayList<Int>>()
    var count = 0

    val pattern = Regex("""^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$""")

    val claims = arrayListOf<Claim>()

    for (line in input) {
        val result = pattern.find(line) ?: throw IllegalArgumentException("Cannot parse $line")
        val (id, left, top, w, h) = result.destructured.toList().map { it.toInt() }

        claims.add(Claim(id, left, top, w, h, false))
    }

    println("Finished claims")

    for (claim in claims) {
        for (x in claim.left until claim.left + claim.w) {
            for (y in claim.top until claim.top + claim.h) {
                val old = area[Pair(x, y)] ?: 0

                area[Pair(x, y)] = old + 1


                if (owners[Pair(x, y)] == null) {
                    owners[Pair(x, y)] = arrayListOf(claim.id)
                } else {
                    owners[Pair(x, y)]?.add(claim.id)
                }

                if (old != 0) {
                    claim.overlap = true

                    val ownerList = owners[Pair(x, y)]
                    if (ownerList != null) {
                        for (o in ownerList) claims.filter { it.id == o }.onEach { it.overlap = true }
                    }
                }
            }
        }
    }

    println("Finished area")

    for (tile in area) {
        if (tile.value > 1) count++
    }

    println("Part 1 : $count")


    for (noover in claims.filter { !it.overlap }) {
        println("Part 2 : no overlap claim = ${noover.id}")
    }
}