import java.io.File

fun main(args: Array<String>) {
    val input = File("src/input_02.txt").readLines()

    var twoCount = 0
    var threeCount = 0

    val sameLetters = Regex("""([a-z])\1+""")

    for (line in input) {
        val sortedLine = line.toCharArray().sorted().joinToString("")

        val matches = sameLetters.findAll(sortedLine)

        var hasTwo = false
        var hasThree = false

        for (matchResult in matches) {
            if (matchResult.value.length == 2) hasTwo = true
            if (matchResult.value.length == 3) hasThree = true
        }

        if (hasTwo) twoCount++
        if (hasThree) threeCount++


        // Part 2
        var diffs : Int
        for (other in input.filter { it != line }) {
            diffs = 0
            for (i in 0 until line.length) {
                if (line[i] != other[i]) diffs++
                if (diffs > 1) continue
            }

            if (diffs == 1) println("Part 2 : $line   $other  $diffs")
        }
    }

    println("Part 1 : " + twoCount * threeCount)
}